#!/bin/sh

LOG_DIR="../logs"
TESTED_DATASET="$LOG_DIR/VOCv1"
LOG_FILE_CPU="$TESTED_DATASET/cpu_usage[total].txt"
LOG_FILE_SYS="$TESTED_DATASET/system_usage[process].txt"
LOG_FILE_GPU="$TESTED_DATASET/gpu_usage[process].txt"
LOG_FILE="$TESTED_DATASET/training.txt"
delay=60


if [ ! -d "$LOG_DIR" ]; then
    mkdir "$LOG_DIR"
fi

if [ ! -d "$TESTED_DATASET" ]; then
    mkdir "$TESTED_DATASET"
fi

# use run command to train or test
./test.sh > $LOG_FILE &
pid=$!

# If this script is killed, kill the `cp'.
trap "kill $pid 2> /dev/null" EXIT

# While copy is running...
while kill -0 $pid 2> /dev/null; do
    top -p $pid -b -n 1 | sed -n '3p' >> $LOG_FILE_CPU
    top -p $pid -b -n 1 | sed -n '8p' >> $LOG_FILE_SYS
    nvidia-smi | grep $pid >> $LOG_FILE_GPU
    sleep $delay
done

# Disable the trap on a normal exit.
trap - EXIT