#!/bin/sh


# Linux
wget=/usr/bin/wget
tar=/bin/tar
unzip=/usr/bin/unzip

# FreeBSD - if use other systems try command whereis
#wget=/usr/local/bin/wget
#tar=/usr/bin/tar
#unzip=/usr/local/bin/unzip

TXT="Download annotations and images of datasets"
ANN_DIR="./annotation"
IMG_DIR="./images"
WORKING_DIR=$(pwd)
COCO_DIR="./COCO"
VOC_DIR="./PASCAL_VOC"
COCO_ZIP_FILE="annotations_trainval2017.zip"
VOC_ZIP_FILE="PASCAL_VOC.zip"
URL_COCO_ANN="http://images.cocodataset.org/annotations/$COCO_ZIP_FILE"
URL_VOC_ANN="https://storage.googleapis.com/coco-dataset/external/$VOC_ZIP_FILE"
TRAIN_FILE="train2017"
VAL_FILE="val2017"
VOC_IMG_FILE="VOCtrainval_11-May-2012.tar"
URL_COCO_IMGS_TRAIN="http://images.cocodataset.org/zips/$TRAIN_FILE.zip"
URL_COCO_IMGS_VAL="http://images.cocodataset.org/zips/$VAL_FILE.zip"
URL_VOC_IMGS_TRAIN_VAL="http://host.robots.ox.ac.uk/pascal/VOC/voc2012/$VOC_IMG_FILE"

if [ ! -x "$wget" ]; then
  echo "ERROR: No wget." >&2
  exit 1
elif [ ! -x "$unzip" ]; then
  echo "ERROR: Not unzip." >&2
  exit 1
elif [ ! -x "$tar" ]; then
  echo "ERROR: Not tar." >&2
  exit 1
fi

echo "$TXT"
echo "$WORKING_DIR"
# change to working directory and cleanup any downloaded files
if [ ! -d "$ANN_DIR" ]; then
    mkdir "$ANN_DIR"
fi
if [ ! -d "$IMG_DIR" ]; then
    mkdir "$IMG_DIR"
fi

cd "$WORKING_DIR"
######################[ PREPARE ANNOTATIONS ]######################
# Delete each file in a loop or create directory if not exist

if ! cd "$ANN_DIR"; then
  echo "ERROR: can't access working directory ($ANN_DIR)" >&2
  exit 1
fi

if [ -d "$COCO_DIR" ]; then
    for file in "$COCO_DIR"/*; do
        rm -f "$file"
    done
else
    mkdir "$COCO_DIR"
fi

# Download and unzip into file required files
#### COCO
if ! ${wget} "$URL_COCO_ANN"; then
  echo "ERROR: can't download file $URL_COCO_ANN" >&2
  exit 1
fi
if [ ! -f "./$COCO_ZIP_FILE" ]; then
  echo "ERROR: I'm confused, where's my archive?" >&2
  exit 1
fi
${unzip} "./$COCO_ZIP_FILE" -d "$COCO_DIR"
cd "$COCO_DIR/annotations"
for file in ./*; do
    mv "$file" "../$file"
done
cd ..
rmdir "./annotations"
cd ..
rm "./$COCO_ZIP_FILE"

#### VOC
cd "$WORKING_DIR"
if ! cd "$ANN_DIR"; then
  echo "ERROR: can't access working directory ($ANN_DIR)" >&2
  exit 1
fi

if ! ${wget} "$URL_VOC_ANN"; then
  echo "ERROR: can't download file $URL_VOC_ANN" >&2
  exit 1
fi
if [ ! -f "./$VOC_ZIP_FILE" ]; then
  echo "ERROR: I'm confused, where's my archive?" >&2
  exit 1
fi
${unzip} "./$VOC_ZIP_FILE"
rm "./$VOC_ZIP_FILE"


######################[ DOWNLOAD IMAGES ]######################
cd "$WORKING_DIR"
#### VOC
if ! cd "$IMG_DIR"; then
  echo "ERROR: can't access working directory ($IMG_DIR)" >&2
  exit 1
fi

if [ ! -d "$VOC_DIR" ]; then
    mkdir "$VOC_DIR"
fi

if ! ${wget} "$URL_VOC_IMGS_TRAIN_VAL"; then
  echo "ERROR: can't download file $URL_VOC_IMGS_TRAIN_VAL" >&2
  exit 1
fi
if [ ! -f "./$VOC_IMG_FILE" ]; then
  echo "ERROR: I'm confused, where's my archive?" >&2
  exit 1
fi
${tar} -xf "./$VOC_IMG_FILE"
rm "./$VOC_IMG_FILE"

# move just imgs into directory
mv "./VOCdevkit/VOC2012/JPEGImages/"* "$VOC_DIR"
rm -rf "./VOCdevkit"

#### COCO
cd "$WORKING_DIR"
if ! cd "$IMG_DIR"; then
  echo "ERROR: can't access working directory ($IMG_DIR)" >&2
  exit 1
fi

if [ ! -d "$COCO_DIR" ]; then
    mkdir "$COCO_DIR"
fi

if ! ${wget} "$URL_COCO_IMGS_VAL"; then
  echo "ERROR: can't download file $URL_COCO_IMGS_VAL" >&2
  exit 1
fi
if [ ! -f "./$VAL_FILE.zip" ]; then
  echo "ERROR: I'm confused, where's my archive?" >&2
  exit 1
fi
${unzip} -q "./$VAL_FILE.zip"
rm "./$VAL_FILE.zip"
# move just imgs into directory
mv "./$VAL_FILE/"* "./$COCO_DIR/"
rm -rf "./$VAL_FILE"

cd "$WORKING_DIR"
if ! cd "$IMG_DIR"; then
  echo "ERROR: can't access working directory ($IMG_DIR)" >&2
  exit 1
fi

if [ ! -d "$COCO_DIR" ]; then
    mkdir "$COCO_DIR"
fi

if ! ${wget} "$URL_COCO_IMGS_TRAIN"; then
  echo "ERROR: can't download file $URL_COCO_IMGS_TRAIN" >&2
  exit 1
fi
if [ ! -f "./$TRAIN_FILE.zip" ]; then
  echo "ERROR: I'm confused, where's my archive?" >&2
  exit 1
fi
${unzip} -q "./$TRAIN_FILE.zip"
rm "./$TRAIN_FILE.zip"
# move just imgs into directory
# mv "./$TRAIN_FILE/"* "./$COCO_DIR/" doesn't work for many images
cd "./$TRAIN_FILE"
find . -name "*.jpg" -exec mv {} "../$COCO_DIR/" \;
cd ..
rm -rf "./$TRAIN_FILE"

echo "All downloads are successfully done"





