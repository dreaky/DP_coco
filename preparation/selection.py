__author__ = 'slezak'
__version__ = '1.0'

import sys
import os
import re
import shutil
import time
from pycocotools.coco import COCO

test = 2
val = 1
train = 0
COCO_DIR = 'COCO'
VOC_DIR = 'PASCAL_VOC'
UP_P = '..'
ANN_P = 'annotations'
LBL_P = 'labels'
TRA_P = 'train2017'
VAL_P = 'val2017'
TST_P = 'test2017'


def load_possible_datasets():
    cwd = os.getcwd()
    datasets = {}

    # print('Curent directory is %s.' % cwd)
    ann_path = os.path.join(cwd, 'annotation')

    index = 1
    for root, dirs, files in os.walk(ann_path):
        # print root
        # print dirs
        # print files
        for anns in dirs:
            if anns != 'COCO' and anns != 'PASCAL_VOC':
                datasets[index] = str(os.path.join(ann_path, anns))
                index += 1

    return datasets


def purge(directory, pattern):
    for f in os.listdir(directory):
        if re.search(pattern, f):
            os.remove(os.path.join(directory, f))


def prepare_directories():
    cwd = os.getcwd()
    tic = time.time()

    # print('Curent directory is %s.' % cwd)
    ann_path = os.path.join(cwd, UP_P, ANN_P)
    train_path = os.path.join(cwd, UP_P, TRA_P)
    val_path = os.path.join(cwd, UP_P, VAL_P)
    test_path = os.path.join(cwd, UP_P, TST_P)
    lbl_path = os.path.join(cwd, UP_P, LBL_P)

    # remove .json files from annotation path
    pattern = ''
    purge(ann_path, pattern)

    # delete a image's directories and all its contents
    if os.path.isdir(train_path):
        shutil.rmtree(train_path)
    if os.path.isdir(val_path):
        shutil.rmtree(val_path)
    if os.path.isdir(test_path):
        shutil.rmtree(test_path)
    # if os.path.isdir(lbl_path):
    #     shutil.rmtree(lbl_path)

    if not os.path.exists(train_path):
        os.makedirs(train_path)
    if not os.path.exists(val_path):
        os.makedirs(val_path)
    if not os.path.exists(test_path):
        os.makedirs(test_path)
    if not os.path.exists(ann_path):
        os.makedirs(ann_path)
    # if not os.path.exists(lbl_path):
    #     os.makedirs(lbl_path)
    # if not os.path.exists(os.path.join(lbl_path, TST_P)):
    #     os.makedirs(os.path.join(lbl_path, TST_P))
    # if not os.path.exists(os.path.join(lbl_path, TRA_P)):
    #     os.makedirs(os.path.join(lbl_path, TRA_P))
    # if not os.path.exists(os.path.join(lbl_path, VAL_P)):
    #     os.makedirs(os.path.join(lbl_path, VAL_P))

    print('Successfully purged all directories. (t=%.2fs)' % (time.time() - tic))


def create_labels(annotations, img, is_voc, path):
    global category
    coco_ids = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 27, 28, 31, 32,
                33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59,
                60, 61, 62, 63, 64, 65, 67, 70, 72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, 84, 85, 86, 87, 88, 89, 90]

    voc_ids = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20]

    file_name = os.path.splitext(img['file_name'])[0] + ".txt"
    full_path = os.path.join(path, file_name)
    dw = 1/float(img['width'])
    dh = 1/float(img['height'])

    # create file with img id
    with open(full_path, 'w') as outfile:
        for ann in annotations:
            if is_voc:
                category = voc_ids.index(int(ann['category_id']))
            else:
                category = coco_ids.index(int(ann['category_id']))
            bbox = ann['bbox']
            x = (bbox[0] + (bbox[2]/2)) * dw
            w = bbox[2] * dw
            y = (bbox[1] + (bbox[3]/2)) * dh
            h = bbox[3] * dh

            outfile.write("%d %f %f %f %f\n" % (category, x, y, w, h))


def copy_and_load_anns(path):
    cwd = os.getcwd()
    tic = time.time()
    print('Current directory is %s.' % cwd)
    # -------- COPY -------- #
    for root, dirs, files in os.walk(path):
        for f in files:
            imgs_paths = []
            dest = os.path.join(cwd, UP_P, ANN_P, f)
            shutil.copyfile(os.path.join(path, f), dest)

            # -------- LOAD -------- #
            json = COCO(dest)
            imgs_dir = os.path.join(cwd, 'images')
            i = 0
            for img in json.imgs:
                i += 1
                done = int(50 * i / len(json.imgs))
                sys.stdout.write("\rCopy.. \t[%s%s] %s/%s"
                                 "" % ('=' * done, ' ' * (50 - done), i, len(json.imgs)))
                sys.stdout.flush()

                imgs_paths.append(copy_imgs(path, json, img, f, imgs_dir, cwd))

            print('')

            file_imgs = os.path.splitext(f)[0] + ".txt"
            with open(os.path.join(cwd, UP_P, file_imgs), 'w') as f:
                for i in imgs_paths:
                    f.write("%s\n" % i)

        print('Images successfully copied')

    print('Successfully loaded annotation and copied images. (t=%.2fs)' % (time.time() - tic))


def copy_imgs(path, json, img_id, fname, imgs_dir, cwd):
    img = json.imgs[img_id][u'file_name']
    dest_dir = ''
    if fname == 'instances_train2017.json':
        dest_dir = TRA_P
    elif fname == 'instances_minival2017.json':
        dest_dir = VAL_P
    elif fname == 'instances_test2017.json':
        dest_dir = TST_P
    else:
        assert 'Unexpected file name.'

    out_path = os.path.join(cwd, UP_P, dest_dir)

    if VOC_DIR in path:
        # it's PASCAL_VOC dataset
        shutil.copyfile(os.path.join(imgs_dir, VOC_DIR, img), os.path.join(out_path, img))
        create_labels(json.imgToAnns[img_id], json.imgs[img_id], True, out_path)
    else:
        # it's COCO dataset
        shutil.copyfile(os.path.join(imgs_dir, COCO_DIR, img), os.path.join(out_path, img))
        create_labels(json.imgToAnns[img_id], json.imgs[img_id], False, out_path)

    return os.path.join(out_path, img)


if __name__ == '__main__':
    prepare_directories()

    datasets = load_possible_datasets()
    print('Here are all loaded datasets, plese select one of them:')
    for key, value in datasets.iteritems():
        print('%d - %s' % (key, value))
    selected = input('Now write number of dataset: ')

    copy_and_load_anns(datasets[selected])

    print('\nDataset ready to training.')
