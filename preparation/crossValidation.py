__author__ = 'slezak'
__version__ = '1.0'

import sys
import os
import json
import time
import operator
import datetime
from pycocotools.coco import COCO

divide_size = 12
count = 4
test = 2
val = 1
train = 0


def load_dataset(val_path, train_path):
    # load annotation files
    actual_dir = os.getcwd()
    ann_dir = os.path.join(actual_dir, 'annotation')
    ann_file = os.path.join(ann_dir, train_path)
    train = COCO(ann_file)
    ann_file = os.path.join(ann_dir, val_path)
    val = COCO(ann_file)

    # merge all attributes
    tic = time.time()
    print('Start merging datasets ...')
    train.anns.update(val.anns)
    train.cats.update(val.cats)
    train.imgs.update(val.imgs)

    i = 0
    for key, value in train.catToImgs.iteritems():
        i += 1
        done = int(50 * i / len(train.catToImgs))
        sys.stdout.write("\rMerging %s \t[%s%s] %s/%s"
                         "" % ('categories to images', '=' * done, ' ' * (50 - done), i, len(train.catToImgs)))
        sys.stdout.flush()

        if type(value) == dict:
            train.catToImgs[key] = train.catToImgs[key].update(val.catToImgs[key])
        elif type(value) == list:
            train.catToImgs[key] = val.catToImgs[key] + [x for x in train.catToImgs[key] if x not in val.catToImgs[key]]
        else:
            assert 'Unexpected type {} is not supported'.format(type(value))

    print('')
    i = 0
    for key, value in val.imgToAnns.iteritems():
        i += 1
        done = int(50 * i / len(val.imgToAnns))
        sys.stdout.write("\rMerging %s \t[%s%s] %s/%s"
                         "" % ('imgs to annotations', '=' * done, ' ' * (50 - done), i, len(val.imgToAnns)))
        sys.stdout.flush()

        train.imgToAnns[key] = value

    print('')
    print('Datasets was merged. (t=%.2fs)' % (time.time() - tic))

    '''
    # test if datasets are merged
    img = train.loadImgs(480985)[0]
    print img
    I = io.imread('%s' % (img['coco_url']))
    fig = plt.figure()
    fig.show()
    print len(train.cats)
    '''

    return train


def distribute_imgs(dataset):
    imgs_in_cats = {}
    indexed_imgs = []
    all_images = []
    for i in range(divide_size):
        tmp = set()
        indexed_imgs.append(tmp)

    tic = time.time()
    print('Start with defining indexes ...')
    for i in dataset.imgs:
        all_images.append(i)

    for cat in dataset.cats:
        imgs_in_cats[cat] = len(dataset.getImgIds(catIds=cat))

    # get all categories from dataset and sort them
    sorted_cats = sorted(imgs_in_cats.items(), key=operator.itemgetter(1))

    tmp = 0
    # insert imgs into indexes from smallest category to biggest
    for cat, size in sorted_cats:
        tmp += 1
        done = int(50 * tmp / len(sorted_cats))
        sys.stdout.write("\rDefining %s \t[%s%s] %s/%s [%d]"
                         "" % ('indexes for images', '=' * done, ' ' * (50 - done), tmp, len(sorted_cats), size))
        sys.stdout.flush()

        imgs = dataset.getImgIds(catIds=cat)
        for i in range(len(imgs) // divide_size):
            for j in range(divide_size):
                img_id = imgs[i * divide_size + j]
                if img_id in all_images:
                    indexed_imgs[j].add(img_id)
                    all_images.remove(img_id)

        for k in range(len(imgs) % divide_size):
            img_id = imgs[len(imgs) - 1 - k]
            if img_id in all_images:
                indexed_imgs[k].add(img_id)
                all_images.remove(img_id)

    print('')
    print('Indexes was defined. (t=%.2fs)' % (time.time() - tic))

    return indexed_imgs


def define_indexes():
    # 0, 1, 2, 3, 4, 5, 6, 7 - train
    # 8 - val
    # 9, 10, 11 - test
    order_indexes = []
    for i in range(count):
        order_indexes.append([[], [], []])

    # can not be used randomly because it creates a wrong distribution
    # random_indexes = random.sample(xrange(0, divide_size), divide_size)

    for i in range(divide_size // 4):
        # fill test
        order_indexes[0][test].append(i)
        order_indexes[1][test].append(divide_size - 1 - i)
        order_indexes[2][test].append(divide_size / 2 - i)
        order_indexes[3][test].append(divide_size // 3 * i)

    for i in range(divide_size // 12):
        # fill val
        order_indexes[0][val].append((divide_size // 4) + i)
        order_indexes[1][val].append(divide_size - (divide_size // 4) - 1 - i)
        order_indexes[2][val].append(i)
        order_indexes[3][val].append(divide_size - 1 - i)

    for i in range(divide_size):
        # fill train
        for j in range(count):
            if not i in order_indexes[j][val] and not i in order_indexes[j][test]:
                order_indexes[j][train].append(i)

    return order_indexes


def merge_imgs(indexes, imgs):
    new = []
    i = 0
    for index in indexes:
        i += 1
        done = int(50 * i / len(indexes))
        sys.stdout.write("\rMerging %s \t[%s%s] %s/%s"
                         "" % ('images by indexes', '=' * done, ' ' * (50 - done), i, len(indexes)))
        sys.stdout.flush()

        new += list(imgs[index])

    print('')
    return new


def create_json_file(dataset, indexes, defined_imgs, i, t, dataset_name):
    file_type = ''
    file_indexes = []
    file_name = ''
    if t == train:
        file_type = 'training'
        file_indexes = indexes[t]
        file_name = 'instances_train2017.json'
    elif t == val:
        file_type = 'validation'
        file_indexes = indexes[t]
        file_name = 'instances_minival2017.json'
    elif t == test:
        file_type = 'testing'
        file_indexes = indexes[t]
        file_name = 'instances_test2017.json'

    result_dir = os.path.join('annotation', '%sv%d' % (dataset_name, int(i) + 1))
    if not os.path.exists(result_dir):
        os.makedirs(result_dir)

    tic = time.time()
    print('Start creating json file for %s datasets ...' % file_type)
    new_dataset = {}
    # copy and update info
    dic_info = {}
    if dataset.dataset.has_key(u'info'):
        str_info = '%s updated for Diploma project where we combination train and val annotation. ' \
                   'This file is contains only %s data by indexes %s of all %d.' % \
                   (dataset.dataset[u'info'][u'description'],
                    file_type,
                    str(file_indexes),
                    divide_size
                    )
        u_date = u'%s' % datetime.datetime.now().strftime('%Y/%m/%d')
        dic_info.update({u'description': str_info})
        dic_info.update({u'author': u'David Slezak'})
        dic_info.update({u'version': u'1.1'})
        dic_info.update({u'year': 2017})
        dic_info.update({u'contributor': dataset.dataset[u'info'][u'contributor']})
        dic_info.update({u'date_created': u_date})
    else:
        str_info = '%s updated for Diploma project where we combination train and val annotation. ' \
                   'This file is contains only %s data by indexes %s of all %d.' % \
                   (dataset_name,
                    file_type,
                    str(file_indexes),
                    divide_size
                    )
        u_date = u'%s' % datetime.datetime.now().strftime('%Y/%m/%d')
        dic_info.update({u'description': str_info})
        dic_info.update({u'author': u'David Slezak'})
        dic_info.update({u'version': u'1.1'})
        dic_info.update({u'year': 2017})
        dic_info.update({u'date_created': u_date})

    new_dataset.update({u'info': dic_info})

    # licences and categories are just copied
    if dataset.dataset.has_key(u'licenses'):
        new_dataset.update({u'licenses': dataset.dataset[u'licenses']})
    if dataset.dataset.has_key(u'categories'):
        new_dataset.update({u'categories': dataset.dataset[u'categories']})

    # define imgs and annotations
    arr_imgs = []
    arr_annot = []
    for img_id in defined_imgs:
        arr_imgs.append(dataset.loadImgs(ids=img_id)[0])
        for annot in dataset.imgToAnns[img_id]:
            arr_annot.append(annot)

    new_dataset.update({u'images': arr_imgs})
    new_dataset.update({u'annotations': arr_annot})

    with open(os.path.join(result_dir, file_name), 'w') as outfile:
        json.dump(new_dataset, outfile)

    print('Successful created .json data file. (t=%.2fs)' % (time.time() - tic))


def load_coco():
    dataset_name = 'COCO'
    train_path = os.path.join(dataset_name, 'instances_train2017.json')
    val_path = os.path.join(dataset_name, 'instances_val2017.json')
    dataset = load_dataset(val_path, train_path)
    imgs = distribute_imgs(dataset)
    # imgs = []
    # imgs.append(set())
    # imgs[0].add(403385)

    # print(imgs)
    for i, indexes in enumerate(define_indexes()):
        # train, val, test
        for t in range(3):
            create_json_file(dataset, indexes, merge_imgs(indexes[t], imgs), i, t, dataset_name)
            # create_json_file(dataset, indexes, [403385], i, t, dataset_name)


def load_voc():
    dataset_name = 'PASCAL_VOC'
    train_path = os.path.join(dataset_name, 'pascal_train2012.json')
    val_path = os.path.join(dataset_name, 'pascal_val2012.json')
    dataset = load_dataset(val_path, train_path)
    imgs = distribute_imgs(dataset)
    # imgs = []
    # imgs.append(set())
    # imgs[0].add(403385)

    # print(imgs)
    for i, indexes in enumerate(define_indexes()):
        # train, val, test
        for t in range(3):
            create_json_file(dataset, indexes, merge_imgs(indexes[t], imgs), i, t, dataset_name)
            # create_json_file(dataset, indexes, [403385], i, t, dataset_name)


load_coco()
load_voc()
