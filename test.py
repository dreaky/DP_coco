from pycocotools.coco import COCO
import numpy as np
import skimage.io as io
import matplotlib.pyplot as plt
import pylab
import os

"""
http://coco.lri.fr/COCOdoc/installation.html
"""

pylab.rcParams['figure.figsize'] = (10.0, 8.0)

dataDir = '.'
dataType = 'minival2017'
imgsDir = 'val2017'
annFile = '%s/annotations/instances_%s.json' % (dataDir, dataType)
resultsDir = 'results'

# initialize COCO api for instance annotations
coco = COCO(annFile)

# display COCO categories and supercategories
cats = coco.loadCats(coco.getCatIds())
nms = [cat['name'] for cat in cats]
print 'COCO categories: \n', ' '.join(nms)
print len(nms)

nms = set([cat['supercategory'] for cat in cats])
print '\nCOCO supercategories: \n', ' '.join(nms)
print len(nms)

# get all images containing given categories, select one at random
catIds = coco.getCatIds(catNms=['person', 'dog', 'skateboard', 'bicycle'])
print catIds
imgIds = coco.getImgIds(catIds=catIds)
print imgIds
img = coco.loadImgs(imgIds[np.random.randint(0, len(imgIds))])[0]
print img

# load and display image
# I = io.imread('%s/images/%s/%s'%(dataDir,dataType,img['file_name']))
# use url to load image
print 'image id is %d' % (img['id'])
I = io.imread('%s' % (os.path.join(dataDir, imgsDir, img[u'file_name'])))
fig = plt.figure()
# plt.axis('off')
# plt.imshow(I)


# load and display instance annotations
plt.imshow(I)
plt.axis('off')
annIds = coco.getAnnIds(imgIds=img['id'], catIds=catIds, iscrowd=None)
anns = coco.loadAnns(annIds)
print anns
#print bboxIds

coco.showBBoxs(anns)
plt.show()

if not os.path.exists(resultsDir):
    os.makedirs(resultsDir)
fig.savefig('./%s/%d.png' % (resultsDir, img['id']))